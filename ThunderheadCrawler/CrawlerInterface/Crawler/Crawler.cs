﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrawlerInterface;
using System.Threading;

namespace CrawlerInterface
{
    public class Crawler
    {
        /// <summary>
        /// Definitions for Crawler.
        /// </summary>
        private Uri CrawlUrl;
        private Indexer Indexer;
        private JobConfig JobConfig;

        private string CrawlerDocHtml = "";
        private HtmlAgilityPack.HtmlDocument CrawlerDocDom = new HtmlAgilityPack.HtmlDocument();

        private int ValidLinks = 0;
        private int InvalidLinks = 0;
        private int IndexedLinks = 0;
        private int ExistingLinks = 0;
        private int LinksRepaired = 0;
        private int LinksBlocked = 0;
        private int CrawlFetches = 0;

        private bool CrawlerPaused = false;
        private bool CrawlerStopped = false;
        private bool DoneFirst = false;

        /// <summary>
        /// Constructor - sets up the initial crawl url and assigns a console to this object.
        /// </summary>
        /// <param name="startUrl"></param>
        public Crawler(JobConfig cfg) 
        {
            CrawlerConsole.CreateConsole();

            this.JobConfig = cfg;
            this.CrawlUrl = new Uri(cfg.GetFullUrl());
            this.Indexer = new Indexer(cfg);
        }

        /// <summary>
        /// Starts the crawler.
        /// </summary>
        public void CrawlerRun()
        {
            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerRun] #####################################");
            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerRun] Starting crawler...");
            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerRun] #####################################");
            this.CrawlerLoop(CrawlUrl);
        }

        /// <summary>
        /// Pauses the crawler.
        /// </summary>
        /// <returns>bool: The current pause state.</returns>
        public bool PauseCrawler()
        {
            CrawlerConsole.WriteLineInfo("[Crawler][PauseCrawler] #####################################");
            CrawlerConsole.WriteLineInfo("[Crawler][PauseCrawler] " + (this.CrawlerPaused ? "Unpausing" : "Pausing") + "...");
            CrawlerConsole.WriteLineInfo("[Crawler][PauseCrawler] #####################################");
            this.CrawlerPaused = !this.CrawlerPaused;
            return this.CrawlerPaused;
        }

        /// <summary>
        /// Stops the crawler running.
        /// </summary>
        /// <returns>bool: The current stop state.</returns>
        public bool StopCrawler()
        {
            CrawlerConsole.WriteLineInfo("[Crawler][StopCrawler]  #####################################");
            CrawlerConsole.WriteLineInfo("[Crawler][StopCrawler]  Stopping crawler...");
            CrawlerConsole.WriteLineInfo("[Crawler][StopCrawler]  #####################################");
            this.CrawlerStopped = true;
            return this.CrawlerStopped;
        }

        /// <summary>
        /// Resets the statistics of the crawler
        /// </summary>
        private void CrawlerResetStats()
        {
            this.CrawlFetches = 0;
            this.ValidLinks = 0;
            this.InvalidLinks = 0;
            this.IndexedLinks = 0;
            this.ExistingLinks = 0;
            this.LinksRepaired = 0;
            this.LinksBlocked = 0;
        }

        /// <summary>
        /// Runs the crawler forever unless paused or stopped.
        /// </summary>
        /// <param name="uri">Uri: The initial URL to crawl.</param>
        public void CrawlerLoop(Uri uri)
        {
            // Stops the UI from being blocked
            Task.Run(async () =>
            {
                // Sets up our initial URL and creates the Indexer which handles DB connection and adding URLs
                DataManager.SetCurrentBaseDomain(uri);

                while (!this.CrawlerStopped)
                {
                    // If this is the first run, we don't care about getting next URL
                    if(!this.DoneFirst)
                    {
                        this.DoneFirst = true;
                    }
                    else
                    {
                        // If this is not the first loop, then we get the next URL to crawl
                        uri = this.Indexer.GetNextUrl();
                    }

                    // If the indexer returns a URL, we can crawl it
                    if(uri != null)
                    {
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] --------------------------------------");
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Crawling URL: " + uri.ToString());

                        // Parse the fetched HTML into a DOM
                        try
                        {

                            using (WebClient wc = new WebClient())
                            {
                                this.CrawlerDocHtml = wc.DownloadString(uri);
                                this.CrawlerDocDom.LoadHtml(this.CrawlerDocHtml);

                                // Get page title
                                HtmlNode titleTags = this.CrawlerDocDom.DocumentNode.SelectSingleNode("//title");
                                if (titleTags != null)
                                {
                                    this.Indexer.AddTitle(uri.ToString(), WebUtility.HtmlDecode(titleTags.InnerText.Trim()));
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                            {
                                var resp = (HttpWebResponse)ex.Response;
                                if (resp.StatusCode == HttpStatusCode.NotFound)
                                {
                                    CrawlerConsole.WriteLineError("[Crawler][CrawlerLoop] Unable to crawl URL: " + uri.ToString());
                                    this.Indexer.DeleteItem(uri.ToString());
                                    continue;
                                }
                            }
                            throw;
                        }

                        // Make sure that the parser returned an actual document
                        if (this.CrawlerDocDom != null)
                        {
                            HtmlNodeCollection linkTags = this.CrawlerDocDom.DocumentNode.SelectNodes("//a[@href]");
                            // Make sure that we have some anchor tags
                            if (linkTags != null)
                            {
                                // Loop over all the tags and pass them to the Indexer so it to validate and add to the database for crawling
                                foreach (HtmlNode linkTag in ((IEnumerable<HtmlNode>)linkTags))
                                {
                                    int additionCode = this.Indexer.IndexUrl(WebUtility.HtmlDecode(linkTag.GetAttributeValue("href", string.Empty)));

                                    switch (additionCode)
                                    {
                                        case 0:
                                            this.InvalidLinks++;
                                            break;
                                        case 1:
                                            this.IndexedLinks++;
                                            this.ValidLinks++;
                                            break;
                                        case 2:
                                            this.ExistingLinks++;
                                            this.ValidLinks++;
                                            break;
                                        case 3:
                                            this.LinksRepaired++;
                                            break;
                                        case 4:
                                            this.LinksBlocked++;
                                            break;
                                    }
                                    this.CrawlFetches++;
                                }
                            }
                        }
                        
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] --------------------------------------");
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Added Links: " + this.IndexedLinks.ToString() + "\n" +
                            "[Crawler][CrawlerLoop] Fixed Links: " + this.LinksRepaired.ToString() + "\n" +
                            "[Crawler][CrawlerLoop] Existing Links: " + this.ExistingLinks.ToString() + "\n" +
                            "[Crawler][CrawlerLoop] Invalid Links" + this.InvalidLinks.ToString() + "\n" +
                            "[Crawler][CrawlerLoop] Blocked Links" + this.LinksBlocked.ToString());
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] --------------------------------------");

                        // Set the current URL to crawled so we don't touch it again
                        this.Indexer.SetCrawled(uri.ToString());
                        this.CrawlerResetStats();
                    }
                    else
                    {
                        // Indexer returned no URLs to craw, exit.
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] GetNextUrl returned no URL to crawl, likely all URLs have been completed.");
                        this.CrawlerStopped = true;
                        break;
                    }
                }

                this.CrawlerPaused = false;
                this.CrawlerStopped = false;

                CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Crawler job for " + this.JobConfig.GetFullUrl() + " has finished");
            });
        }
        
        /// <summary>
        /// Logs this crawler's statistics
        /// </summary>
        public void ShowStats()
        {
            this.Indexer.ShowStats();
        }

        /// <summary>
        /// Returns the current job configuration
        /// </summary>
        /// <returns>JobConfig: the configuration</returns>
        public JobConfig GetJobConfig()
        {
            return this.JobConfig;
        }

        /// <summary>
        /// Checks if this current instance is running or not (both paused and stopped states)
        /// </summary>
        /// <returns></returns>
        public bool IsRunning()
        {
            return (!this.CrawlerPaused && !this.CrawlerStopped);
        }

        /// <summary>
        /// Returns the state of the crawler, if it is paused or not
        /// </summary>
        /// <returns>bool: the pause state</returns>
        public bool IsPaused()
        {
            return this.CrawlerPaused;
        }

        /// <summary>
        /// Returns the state of the crawler, if it is stopped or not
        /// </summary>
        /// <returns>bool: the stopped state</returns>
        public bool IsStopped()
        {
            return this.CrawlerStopped;
        }
    }
}
