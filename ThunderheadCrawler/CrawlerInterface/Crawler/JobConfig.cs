﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerInterface
{
    public class JobConfig
    {
        private string BaseCrawlUrl;
        private bool CrawlSubdomains;
        private bool PreviousJob;

        public JobConfig(string url, bool subdomains, bool prevjob)
        {
            this.BaseCrawlUrl = url;
            this.CrawlSubdomains = subdomains;
            this.PreviousJob = prevjob;
        }

        public string GetBaseDomain()
        {
            return new Uri(this.BaseCrawlUrl).Host.Replace("www.", "");
        }

        public string GetFullUrl()
        {
            return this.BaseCrawlUrl;
        }

        public bool CanCrawlSubdomains()
        {
            return this.CrawlSubdomains;
        }

        public bool IsPreviousJob()
        {
            return this.PreviousJob;
        }
    }
}
