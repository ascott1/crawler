﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrawlerInterface;

namespace CrawlerInterface
{
    class Indexer
    {
        /// <summary>
        /// Definitions for Indexer.
        /// </summary>
        private DatabaseManager MasterDatabase = new DatabaseManager();
        private bool DatabaseConnected = false;
        private int TruncateSize = 40;
        private JobConfig JobConfig;

        private string[] IgnoredFileTypes = new string[] { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".ttf", ".zip", ".7z", ".bz2", ".tar", ".exe", ".dll", ".pdf", ".psd" };

        /// <summary>
        /// Constructor - creates or connects to database using the domain name.
        /// </summary>
        /// <param name="cfg">JobConfig: the configuration for the job</param>
        public Indexer(JobConfig cfg)
        {
            this.JobConfig = cfg;
            this.DatabaseConnected = this.MasterDatabase.Connect("C:\\SearchEngine\\", this.JobConfig.GetBaseDomain() + ".db");

            if (this.MasterDatabase.Select("SELECT * FROM `se_crawl_job_config` WHERE 1;", null).Rows.Count == 0)
            {
                // Add the URL to the database
                this.MasterDatabase.Insert("INSERT INTO se_crawl_job_config(config_subdomains, config_url) VALUES(@ConfigSubdomains, @ConfigUrl)", new SQLiteParameter[]
                {
                    new SQLiteParameter("ConfigUrl", cfg.GetFullUrl()),
                    new SQLiteParameter("ConfigSubdomains", cfg.CanCrawlSubdomains())
                });
            }
        }

        /// <summary>
        /// Truncates long URLs just for readability from the output.
        /// </summary>
        /// <param name="value">string: The URL</param>
        /// <param name="maxLength">int: The maximum characters allowed</param>
        /// <returns></returns>
        public string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return value.Length <= maxLength ? value : value.Substring(0, maxLength) + " [...]";
        }

        /// <summary>
        /// Gets the next URL from database which has not been crawled and sends it back to the crawler.
        /// </summary>
        /// <returns>uri: If there is a valid URI | null: No URLs left to crawl</returns>
        public Uri GetNextUrl()
        {
            DataTable dataTable = this.MasterDatabase.Select("SELECT * FROM se_indexed_url WHERE url_crawled = 0 LIMIT 1", null);
            if (dataTable.Rows.Count > 0)
            {
                IEnumerator enumerator = dataTable.Rows.GetEnumerator();
                if (enumerator.MoveNext())
                {
                    DataRow dataRow = (DataRow)enumerator.Current;
                    return new Uri(dataRow["url_content"].ToString());
                }
            }
            return null;
        }

        /// <summary>
        /// Sets a URL's status to crawled.
        /// </summary>
        /// <param name="url">string: The url to update</param>
        public void SetCrawled(string url)
        {
            this.MasterDatabase.Update("UPDATE se_indexed_url SET url_crawled = 1 WHERE url_content = @UrlContent", new SQLiteParameter[]
            {
                new SQLiteParameter("UrlContent", url)
            });
        }

        /// <summary>
        /// Handles the addition of URLs to the database. 
        /// This function ensures that URLs are valid.
        /// Some URLs may also come in as the pathname only, so it will attempt to repair and re-validate the URL.
        /// </summary>
        /// <param name="url">string: The URL to index</param>
        /// <returns></returns>
        public int IndexUrl(string url)
        {
            int additionCode = -1;

            bool validUrl = false;
            bool canCrawlUrl = false;
            bool canFix = false;

            CrawlerConsole.WriteLineInfo("[Indexer][IndexUrl]    |--> Adding URL: " + Truncate(url, this.TruncateSize) + " to index");

            // Is this URL in the block list?
            foreach (string value in this.IgnoredFileTypes)
            {
                if (url.Contains(value))
                {
                    return 4;
                }
            }

            // Remove hash parameters from URLs
            if (url.StartsWith("#"))
            {
                return 4;
            }
            if (url.Split('#').Length == 2)
            {
                url = url.Split('#')[0];
            }

            try
            {
                // Try parse URL into a valid format
                Uri uri;
                validUrl = (Uri.TryCreate(url, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps));
                //MessageBox.Show(uri.ToString() + "\n" + url);
                //MessageBox.Show(validUrl.ToString());
            }
            catch (Exception ex)
            {
            }

            // Now check if it contains http and we are targeting the correct host, 
            try
            {
                // Are we crawling subdomains?
                if (this.JobConfig.CanCrawlSubdomains())
                {
                    // Allow all subdomains on the base domain
                    canCrawlUrl = (url.Contains(new Uri(this.JobConfig.GetFullUrl()).Host.Replace("www.", "")) && url.StartsWith("http"));
                }
                else
                {
                    // Only allow base domain only
                    canCrawlUrl = (new Uri(url).Host == new Uri(this.JobConfig.GetFullUrl()).Host && url.StartsWith("http"));
                }
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[Indexer][IndexUrl]    |--> Unable to validate: " + Truncate(url, this.TruncateSize));
            }

            //MessageBox.Show(validUrl.ToString() + " | " + canCrawlUrl.ToString());

            if (canCrawlUrl)
            {
                // If the previous checks are valid, the URL will add normally to the database
                if (this.MasterDatabase.Select("SELECT * FROM se_indexed_url WHERE url_content = @UrlContent", new SQLiteParameter[] { new SQLiteParameter("UrlContent", new Uri(url).ToString()) }).Rows.Count == 0)
                {
                    // Add the URL to the database
                    this.MasterDatabase.Insert("INSERT INTO se_indexed_url(url_content, url_crawled) VALUES(@UrlContent, @UrlCrawled)", new SQLiteParameter[]
                    {
                        new SQLiteParameter("UrlContent", new Uri(url).ToString()),
                        new SQLiteParameter("UrlCrawled", "0"),
                    });

                    CrawlerConsole.WriteLineSuccess("[Indexer][IndexUrl]    |--> Added URL: " + Truncate(url, this.TruncateSize));
                    additionCode = 1;
                }
                else
                {
                    // URL already exists in the database
                    CrawlerConsole.WriteLineWarning("[Indexer][IndexUrl]    |--> Unable to add URL: " + Truncate(url, this.TruncateSize) + ", already exists");
                    additionCode = 2;
                }
            }
            else
            {
                // If the previous checks are not valid, we can likely repair the URL before discarding it
                try
                {
                    CrawlerConsole.WriteLineInfo("[Indexer][IndexUrl]    |--> Attempting fix URL: " + Truncate(url, this.TruncateSize) + " on " + this.JobConfig.GetFullUrl());
                    if(!validUrl)
                    {
                        if (this.MasterDatabase.Select("SELECT * FROM se_indexed_url WHERE url_content = @UrlContent", new SQLiteParameter[] { new SQLiteParameter("UrlContent", new Uri(new Uri(this.JobConfig.GetFullUrl()), url).ToString()) }).Rows.Count == 0)
                        {
                            // If the fix is successful, then it can be added to the list.
                            this.MasterDatabase.Insert("INSERT INTO se_indexed_url(url_content, url_crawled) VALUES(@UrlContent, @UrlCrawled);", new SQLiteParameter[]
                            {
                                new SQLiteParameter("UrlContent", new Uri(new Uri(this.JobConfig.GetFullUrl()), url).ToString()),
                                new SQLiteParameter("UrlCrawled", "0"),
                            });

                            CrawlerConsole.WriteLineSuccess("[Indexer][IndexUrl]    |--> Added URL: " + Truncate(new Uri(new Uri(this.JobConfig.GetFullUrl()), url).ToString(), this.TruncateSize));
                            additionCode = 3;
                        }
                        else
                        {
                            // If the fix was valid, but already exists
                            CrawlerConsole.WriteLineWarning("[Indexer][IndexUrl]    |--> Unable to add URL: " + Truncate(new Uri(new Uri(this.JobConfig.GetFullUrl()), url).ToString(), this.TruncateSize) + ", already exists");
                            additionCode = 2;
                        }
                    }
                    else if (this.JobConfig.CanCrawlSubdomains() && validUrl)
                    {
                        // Add the URL to the database
                        if (this.MasterDatabase.Select("SELECT * FROM se_indexed_url WHERE url_content = @UrlContent", new SQLiteParameter[] { new SQLiteParameter("UrlContent", new Uri(url).ToString()) }).Rows.Count == 0)
                        {
                            this.MasterDatabase.Insert("INSERT INTO se_indexed_url(url_content, url_crawled) VALUES(@UrlContent, @UrlCrawled);", new SQLiteParameter[]
                            {
                                new SQLiteParameter("UrlContent", new Uri(url).ToString()),
                                new SQLiteParameter("UrlCrawled", "0"),
                            });
                        }

                        CrawlerConsole.WriteLineSuccess("[Indexer][IndexUrl]    |--> Added subdomain URL: " + Truncate(url, this.TruncateSize));
                        additionCode = 1;
                    }
                    else
                    {
                        // If the fix was valid, but already exists
                        CrawlerConsole.WriteLineWarning("[Indexer][IndexUrl]    |--> Unable to add URL: " + Truncate(new Uri(new Uri(this.JobConfig.GetFullUrl()), url).ToString(), this.TruncateSize) + ", not crawling subdomains.");
                        additionCode = 4;
                    }
                }
                catch (Exception ex)
                {
                    // If the fix was not possible
                    CrawlerConsole.WriteLineError("[Indexer][IndexUrl]    |--> Failed to fix URL: " + Truncate(url, this.TruncateSize) + ", logging to malformed URLs list");

                    this.MasterDatabase.Insert("INSERT INTO se_indexed_malformed(malformed_url_id, malformed_url_content) VALUES(@MfUrlId, @MfUrlContent)", new SQLiteParameter[]
                    {
                        new SQLiteParameter("MfUrlId", null),
                        new SQLiteParameter("MfUrlContent", url)
                    });

                    additionCode = 0;
                }
            }

            CrawlerConsole.WriteLineInfo("[Indexer][IndexUrl]    |--> Task returned Addition Code: " + additionCode);
            return additionCode;
        }

        public void DeleteItem(string url)
        {
            try
            {
                CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Removing invalid (404) url: " + Truncate(url, this.TruncateSize));
                this.MasterDatabase.Update("DELETE FROM `se_indexed_url` WHERE `url_content` = @UrlContent", new SQLiteParameter[]
                {
                    new SQLiteParameter("UrlContent", url)
                });
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
        
        public void AddTitle(string url, string title)
        {
            try
            {
                CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Adding title info for " + Truncate(url, this.TruncateSize) + " with the title " + Truncate(title, this.TruncateSize));
                this.MasterDatabase.Update("UPDATE `se_indexed_url` SET `url_title` = @UrlTitle WHERE `url_content` = @UrlContent;", new SQLiteParameter[]
                {
                    new SQLiteParameter("UrlTitle", title),
                    new SQLiteParameter("UrlContent", url)
                });
            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Logs this indexer's stats for the crawler (mainly called by the Crawler only)
        /// </summary>
        public void ShowStats()
        {
            DataTable configData = this.MasterDatabase.Select("SELECT * FROM `se_crawl_job_config` WHERE 1;", null);
            DataTable urlCount = this.MasterDatabase.Select("SELECT COUNT(*) AS `url_count` FROM `se_indexed_url` WHERE 1;", null);
            DataTable doneCount = this.MasterDatabase.Select("SELECT COUNT(*) AS `url_done` FROM `se_indexed_url` WHERE `url_crawled` = 1;", null);

            if (configData.Rows.Count > 0)
            {
                IEnumerator configDataEnum = configData.Rows.GetEnumerator();
                IEnumerator urlCountEnum = urlCount.Rows.GetEnumerator();
                IEnumerator doneCountEnum = doneCount.Rows.GetEnumerator();

                CrawlerConsole.WriteLine("[Crawler][StopCrawler]  #####################################");

                if (configDataEnum.MoveNext() && urlCountEnum.MoveNext() && doneCountEnum.MoveNext())
                {
                    DataRow configDataData = (DataRow)configDataEnum.Current;
                    DataRow urlCountData = (DataRow)urlCountEnum.Current;
                    DataRow doneCountData = (DataRow)doneCountEnum.Current;

                    CrawlerConsole.WriteLine("[Crawler][ShowStats]    Website URL: " + configDataData["config_url"].ToString());
                    CrawlerConsole.WriteLine("[Crawler][ShowStats]    Crawling Subdomains: " + configDataData["config_subdomains"].ToString());
                    CrawlerConsole.WriteLine("[Crawler][ShowStats]  Crawled URLs: " + doneCountData["url_done"].ToString() + " out of " + urlCountData["url_count"].ToString() + " found URLs");
                }

                CrawlerConsole.WriteLine("[Crawler][StopCrawler]  #####################################");
            }
        }
    }
}
