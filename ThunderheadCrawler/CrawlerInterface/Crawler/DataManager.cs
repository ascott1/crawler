﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrawlerInterface;

namespace CrawlerInterface
{
    class DataManager
    {
        private static List<Uri> BaseDomainList = new List<Uri>();
        private static Uri CurrentBaseDomain;
        private static bool CrawlSubdomains = false;
        private static string DefaultDatabaseFolder = "C:\\SearchEngine\\";

        public static List<Uri> GetUrlList()
        {
            return BaseDomainList;
        }

        public static void AddUrlItem(string item)
        {
            try
            {
                if (!BaseDomainList.Contains(new Uri(item)))
                {
                    CrawlerConsole.WriteLineInfo("[DataManager][AddUrlItem] Added URL to list");
                    BaseDomainList.Add(new Uri(item));
                }
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[DataManager][AddUrlItem] Unable to add Url to list, invalid");
            }
        }

        public static bool RemoveUrlItem(string item)
        {
            return BaseDomainList.Remove(new Uri(item));
        }

        public static Uri GetCurrentBaseDomain()
        {
            return CurrentBaseDomain;
        }

        public static void SetCurrentBaseDomain(Uri domain)
        {
            CurrentBaseDomain = domain;
        }

        public static bool CanCrawlSubdomains()
        {
            return CrawlSubdomains;
        }

        public static bool ToggleCrawlSubdomains()
        {
            CrawlSubdomains = !CrawlSubdomains;
            return CrawlSubdomains;
        }

        public static string GetDatabaseFolder()
        {
            return DefaultDatabaseFolder;
        }

        public static void SetDatabaseFolder(string f)
        {
            DefaultDatabaseFolder = f;
        }
    }
}
