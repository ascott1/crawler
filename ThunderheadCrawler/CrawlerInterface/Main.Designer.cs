﻿namespace CrawlerInterface
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbAddUrl = new System.Windows.Forms.TextBox();
            this.btAddUrl = new System.Windows.Forms.Button();
            this.lbUrlList = new System.Windows.Forms.ListBox();
            this.btStartCrawler = new System.Windows.Forms.Button();
            this.btStopCrawler = new System.Windows.Forms.Button();
            this.ssMainStatus = new System.Windows.Forms.StatusStrip();
            this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.cbCrawlSubdomains = new System.Windows.Forms.CheckBox();
            this.lbPreviousJobs = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ssMainStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbAddUrl
            // 
            this.tbAddUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAddUrl.Location = new System.Drawing.Point(13, 13);
            this.tbAddUrl.Name = "tbAddUrl";
            this.tbAddUrl.Size = new System.Drawing.Size(345, 22);
            this.tbAddUrl.TabIndex = 0;
            this.tbAddUrl.Text = "https://www.domain.co.uk/";
            this.tbAddUrl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddUrl_KeyPress);
            // 
            // btAddUrl
            // 
            this.btAddUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAddUrl.Location = new System.Drawing.Point(364, 12);
            this.btAddUrl.Name = "btAddUrl";
            this.btAddUrl.Size = new System.Drawing.Size(75, 24);
            this.btAddUrl.TabIndex = 1;
            this.btAddUrl.Text = "Add Link";
            this.btAddUrl.UseVisualStyleBackColor = true;
            this.btAddUrl.Click += new System.EventHandler(this.btAddUrl_Click);
            // 
            // lbUrlList
            // 
            this.lbUrlList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbUrlList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUrlList.FormattingEnabled = true;
            this.lbUrlList.ItemHeight = 16;
            this.lbUrlList.Location = new System.Drawing.Point(13, 42);
            this.lbUrlList.Name = "lbUrlList";
            this.lbUrlList.Size = new System.Drawing.Size(426, 100);
            this.lbUrlList.TabIndex = 2;
            this.lbUrlList.SelectedIndexChanged += new System.EventHandler(this.lbUrlList_SelectedIndexChanged);
            // 
            // btStartCrawler
            // 
            this.btStartCrawler.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btStartCrawler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStartCrawler.Location = new System.Drawing.Point(15, 300);
            this.btStartCrawler.Name = "btStartCrawler";
            this.btStartCrawler.Size = new System.Drawing.Size(211, 24);
            this.btStartCrawler.TabIndex = 3;
            this.btStartCrawler.Text = "Start Crawler";
            this.btStartCrawler.UseVisualStyleBackColor = true;
            this.btStartCrawler.Click += new System.EventHandler(this.btStartCrawler_Click);
            // 
            // btStopCrawler
            // 
            this.btStopCrawler.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btStopCrawler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStopCrawler.Location = new System.Drawing.Point(228, 300);
            this.btStopCrawler.Name = "btStopCrawler";
            this.btStopCrawler.Size = new System.Drawing.Size(211, 24);
            this.btStopCrawler.TabIndex = 5;
            this.btStopCrawler.Text = "Stop Crawler";
            this.btStopCrawler.UseVisualStyleBackColor = true;
            this.btStopCrawler.Click += new System.EventHandler(this.btStopCrawler_Click);
            // 
            // ssMainStatus
            // 
            this.ssMainStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatus});
            this.ssMainStatus.Location = new System.Drawing.Point(0, 338);
            this.ssMainStatus.Name = "ssMainStatus";
            this.ssMainStatus.Size = new System.Drawing.Size(451, 22);
            this.ssMainStatus.TabIndex = 6;
            // 
            // tsStatus
            // 
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(42, 17);
            this.tsStatus.Text = "Ready!";
            // 
            // cbCrawlSubdomains
            // 
            this.cbCrawlSubdomains.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCrawlSubdomains.AutoSize = true;
            this.cbCrawlSubdomains.Location = new System.Drawing.Point(13, 271);
            this.cbCrawlSubdomains.Name = "cbCrawlSubdomains";
            this.cbCrawlSubdomains.Size = new System.Drawing.Size(113, 17);
            this.cbCrawlSubdomains.TabIndex = 7;
            this.cbCrawlSubdomains.Text = "Crawl Subdomains";
            this.cbCrawlSubdomains.UseVisualStyleBackColor = true;
            this.cbCrawlSubdomains.CheckedChanged += new System.EventHandler(this.cbCrawlSubdomains_CheckedChanged);
            // 
            // lbPreviousJobs
            // 
            this.lbPreviousJobs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPreviousJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPreviousJobs.FormattingEnabled = true;
            this.lbPreviousJobs.ItemHeight = 16;
            this.lbPreviousJobs.Location = new System.Drawing.Point(13, 165);
            this.lbPreviousJobs.Name = "lbPreviousJobs";
            this.lbPreviousJobs.Size = new System.Drawing.Size(426, 100);
            this.lbPreviousJobs.TabIndex = 8;
            this.lbPreviousJobs.SelectedIndexChanged += new System.EventHandler(this.lbPreviousJobs_SelectedIndexChanged);
            this.lbPreviousJobs.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbPreviousJobs_MouseDoubleClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "Previous Jobs";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 360);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbPreviousJobs);
            this.Controls.Add(this.cbCrawlSubdomains);
            this.Controls.Add(this.ssMainStatus);
            this.Controls.Add(this.btStartCrawler);
            this.Controls.Add(this.lbUrlList);
            this.Controls.Add(this.btAddUrl);
            this.Controls.Add(this.tbAddUrl);
            this.Controls.Add(this.btStopCrawler);
            this.MinimumSize = new System.Drawing.Size(467, 398);
            this.Name = "Main";
            this.Text = "Thunderhead Crawler";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ssMainStatus.ResumeLayout(false);
            this.ssMainStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAddUrl;
        private System.Windows.Forms.Button btAddUrl;
        private System.Windows.Forms.ListBox lbUrlList;
        private System.Windows.Forms.Button btStartCrawler;
        private System.Windows.Forms.Button btStopCrawler;
        private System.Windows.Forms.StatusStrip ssMainStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsStatus;
        private System.Windows.Forms.CheckBox cbCrawlSubdomains;
        private System.Windows.Forms.ListBox lbPreviousJobs;
        private System.Windows.Forms.Label label1;
    }
}

