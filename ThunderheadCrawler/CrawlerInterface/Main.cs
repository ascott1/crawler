﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrawlerInterface;

namespace CrawlerInterface
{
    public partial class Main : Form
    {
        private Dictionary<string, Crawler> CrawlerList = new Dictionary<string, Crawler>();
        private Dictionary<string, JobConfig> PreviousJobsList = new Dictionary<string, JobConfig>();
        private string SelectedIndex = "";

        public Main()
        {
            InitializeComponent();
            ToggleButtons(false, false);
        }

        private void ToggleButtons(bool start, bool stop)
        {
            this.btStartCrawler.Enabled = start;
            this.btStopCrawler.Enabled = stop;
        }

        private void AddItem(string url)
        {
            if (!url.StartsWith("http"))
            {
                url = "http://" + url;
            }

            if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute) && !lbUrlList.Items.Contains(url))
            {
                DataManager.AddUrlItem(url);

                this.lbUrlList.Items.Add(url);
                this.tbAddUrl.Text = "";
                this.tsStatus.Text = "Added URL to scan list.";
            }
            else
            {
                this.tsStatus.Text = "URL is not valid or already exists in the list.";
            }
        }

        private void btAddUrl_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddItem(new Uri(tbAddUrl.Text).GetLeftPart(UriPartial.Authority));
            }
            catch (Exception ex)
            {

            }
        }

        private void tbAddUrl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                try
                {
                    this.AddItem(new Uri(tbAddUrl.Text).GetLeftPart(UriPartial.Authority));
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void btStartCrawler_Click(object sender, EventArgs e)
        {
            ToggleButtons(false, true);

            this.tsStatus.Text = "Crawling: " + this.SelectedIndex;

            if (!this.CrawlerList.ContainsKey(this.SelectedIndex))
            {
                this.CrawlerList.Add(this.SelectedIndex, new Crawler(new JobConfig(this.SelectedIndex, this.cbCrawlSubdomains.Checked, false)));
            }

            this.CrawlerList[this.SelectedIndex].CrawlerRun();
        }

        private void btStopCrawler_Click(object sender, EventArgs e)
        {
            if (this.CrawlerList.ContainsKey(this.SelectedIndex))
            {
                if (this.CrawlerList[SelectedIndex].StopCrawler())
                {
                    if(this.CrawlerList.ContainsKey(this.SelectedIndex))
                    {
                        ToggleButtons(true, false);
                    }
                    this.CrawlerList.Remove(this.SelectedIndex);
                }
                else
                {
                    ToggleButtons(false, true);
                }
            }
        }

        private void cbCrawlSubdomains_CheckedChanged(object sender, EventArgs e)
        {
            this.cbCrawlSubdomains.Checked = DataManager.ToggleCrawlSubdomains();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if(Directory.Exists(DataManager.GetDatabaseFolder()))
            {
                string[] files = Directory.GetFiles(DataManager.GetDatabaseFolder(), "*.db");

                foreach(string f in files)
                {
                    DatabaseManager db = new DatabaseManager();
                    db.Connect(DataManager.GetDatabaseFolder(), Path.GetFileName(f));
                    try
                    { 
                        db.Connect(DataManager.GetDatabaseFolder(), Path.GetFileName(f));

                        DataTable urlCount = db.Select("SELECT COUNT(*) AS `url_count` FROM `se_indexed_url` WHERE 1;", null);
                        DataTable doneCount = db.Select("SELECT COUNT(*) AS `url_done` FROM `se_indexed_url` WHERE `url_crawled` = 1;", null);
                        DataTable configData = db.Select("SELECT * FROM `se_crawl_job_config` WHERE 1;", null);

                        if (urlCount.Rows.Count > 0 && doneCount.Rows.Count > 0 && configData.Rows.Count > 0)
                        {
                            IEnumerator urlCountEnum = urlCount.Rows.GetEnumerator();
                            IEnumerator doneCountEnum = doneCount.Rows.GetEnumerator();
                            IEnumerator configDataEnum = configData.Rows.GetEnumerator();
                            if (urlCountEnum.MoveNext() && doneCountEnum.MoveNext() && configDataEnum.MoveNext())
                            {
                                DataRow urlCountData = (DataRow)urlCountEnum.Current;
                                DataRow doneCountData = (DataRow)doneCountEnum.Current;
                                DataRow configDataData = (DataRow)configDataEnum.Current;

                                bool crawlSubdomains = (configDataData["config_subdomains"].ToString() == "1");
                                
                                this.PreviousJobsList.Add(configDataData["config_url"].ToString(), new JobConfig(configDataData["config_url"].ToString(), crawlSubdomains, true));
                                this.lbPreviousJobs.Items.Add(configDataData["config_url"].ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to load a previous job from: " + f);
                    }
                    finally
                    {
                        db.Close();
                    }
                }
            }
        }

        private void lbPreviousJobs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.lbPreviousJobs.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                DialogResult dialogResult = MessageBox.Show("Continue job for: \n\n\tWebsite URL: " + this.PreviousJobsList[this.SelectedIndex].GetFullUrl() + "\n\tCrawl Subdomains: " + (this.PreviousJobsList[this.SelectedIndex].CanCrawlSubdomains() ? "Yes" : "No"), "Continue Job", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ToggleButtons(false, true);

                    this.tsStatus.Text = "Resuming job: " + this.PreviousJobsList[this.SelectedIndex].GetBaseDomain();

                    if (!this.CrawlerList.ContainsKey(this.SelectedIndex))
                    {
                        this.CrawlerList.Add(this.SelectedIndex, new Crawler(this.PreviousJobsList[this.SelectedIndex]));
                    }

                    this.CrawlerList[this.SelectedIndex].CrawlerRun();
                }
            }
        }

        private void btShowJobStats_Click(object sender, EventArgs e)
        {
            if(this.CrawlerList.ContainsKey(this.SelectedIndex))
            {
                this.CrawlerList[this.SelectedIndex].ShowStats();
            }
        }

        private void lbUrlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.SelectedIndex = lbUrlList.Items[lbUrlList.SelectedIndex].ToString();
                this.tsStatus.Text = "Selecting new job item: " + this.SelectedIndex;
                this.lbPreviousJobs.ClearSelected();

                if (this.CrawlerList.ContainsKey(this.SelectedIndex))
                {
                    ToggleButtons(false, true);
                }
                else
                {
                    ToggleButtons(true, false);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void lbPreviousJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.SelectedIndex = lbPreviousJobs.Items[lbPreviousJobs.SelectedIndex].ToString();
                this.lbUrlList.ClearSelected();

                if(this.CrawlerList.ContainsKey(this.SelectedIndex))
                {
                    this.tsStatus.Text = "Selecting previous job item: " + this.SelectedIndex + ", subdomains:" + this.PreviousJobsList[this.SelectedIndex].CanCrawlSubdomains().ToString();
                    ToggleButtons(false, true);
                }
                else
                {
                    ToggleButtons(true, false);
                }
            }
            catch(Exception ex)
            {
            }
        }
    }
}
