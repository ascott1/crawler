﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThunderheadCrawler
{
    class DataManager
    {
        private static List<Uri> BaseDomainList;

        public static List<Uri> GetUriList()
        {
            return BaseDomainList;
        }

        public static void AddUriItem(string item)
        {
            try
            {
                if (!BaseDomainList.Contains(new Uri(item)))
                {
                    BaseDomainList.Add(new Uri(item));
                }
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[DataManager][AddUriItem] Unable to add URI to list, invalid.");
            }
        }

        public static bool RemoveUriItem(string item)
        {
            return BaseDomainList.Remove(new Uri(item));
        }
    }
}
