﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ThunderheadCrawler
{
    public class Crawler
    {
        private Uri CrawlUri;

        private string CrawlerDocHtml = "";
        private HtmlDocument CrawlerDocDom = new HtmlDocument();

        private Indexer Indexer = new Indexer();

        private string[] IgnoredFileTypes = new string[]
        {
            ".jpg",
            ".jpeg",
            ".png",
            ".bmp",
            ".gif",
            ".ttf",
            ".zip",
            ".7z",
            ".bz2",
            ".tar",
            ".exe",
            ".dll",
            ".pdf",
            ".psd"
        };

        private int AdditionCode = 0;
        private int ValidLinks = 0;
        private int InvalidLinks = 0;
        private int IndexedLinks = 0;
        private int ExistingLinks = 0;
        private int LinksRepaired = 0;
        private int CrawlFetches = 0;

        private bool CrawlerPaused = false;
        private bool CrawlerStopped = false;
        private bool DoneFirst = false;

        public Crawler(string startUri) 
        {
            this.CrawlUri = new Uri(startUri);
        }

        public void CrawlerRun()
        {
            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerRun] Starting crawler...");
            this.CrawlerLoop(CrawlUri);
        }

        public void CrawlerLoop(Uri uri)
        {
            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerRun] Running crawler.");
            while (true)
            {
                while (!CrawlerPaused)
                {
                    if(DoneFirst)
                    {
                        // Crawl first URL to populate database
                        DoneFirst = true;
                    }
                    else
                    {
                        uri = this.Indexer.GetNextUri();
                    }

                    if(uri != null)
                    {
                        try
                        {
                            this.CrawlerDocHtml = new WebClient().DownloadString(uri);
                            this.CrawlerDocDom.LoadHtml(this.CrawlerDocHtml);
                        }
                        catch (Exception ex)
                        {
                            CrawlerConsole.WriteLineError("[Crawler][CrawlerLoop] Unable to crawl url " + uri.ToString() + ex.GetBaseException());
                        }

                        (from n in this.CrawlerDocDom.DocumentNode.Descendants() where n.Name == "script" || n.Name == "style" select n).ToList<HtmlNode>().ForEach(delegate (HtmlNode n)
                        {
                            n.Remove();
                        });

                        if (this.CrawlerDocDom != null)
                        {
                            HtmlNodeCollection LinkTags = this.CrawlerDocDom.DocumentNode.SelectNodes("//a[@href]");
                            if (LinkTags != null)
                            {
                                foreach (HtmlNode LinkTag in ((IEnumerable<HtmlNode>)LinkTags))
                                {
                                    string LinkTagHref = LinkTag.GetAttributeValue("href", string.Empty);
                                    int AdditionCode = this.Indexer.IndexUri(LinkTagHref.ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] GetNextUri returned no uri to crawl, crawling has finished.");
                        CrawlerStopped = true;
                        break;
                    }
                }

                if (CrawlerStopped)
                {
                    break;
                }
            }

            CrawlerConsole.WriteLineInfo("[Crawler][CrawlerLoop] Stopped crawler.");
        }
    }
}
