﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace ThunderheadCrawler
{
    class DatabaseManager
    {
        private SQLiteConnection MasterDatabase = new SQLiteConnection();

        /// <summary>
        /// Connect to a local SQLiteDatabase file on the disk.
        /// </summary>
        /// <param name="path">string: The path to the file (excluding file name).</param>
        /// <param name="database">string: The file name.</param>
        /// <returns>bool: If the connection was successful or not.</returns>
        public bool Connect(string path, string database)
        {
            try
            {
                if(!Directory.Exists(path))
                {
                    Console.WriteLine("[Database.cs][Connect] Creating new database directory: " + path);
                    Directory.CreateDirectory(path);
                }

                if(!File.Exists(path + database))
                {
                    Console.WriteLine("[Database.cs][Connect] Creating new database file: " + database + " in " + path);
                    SQLiteConnection.CreateFile(path + database);
                }

                this.MasterDatabase = new SQLiteConnection("Data Source=" + path + database + ";Version=3;");
                this.MasterDatabase.Open();

                CrawlerConsole.WriteLineInfo("[Database.cs][Connect] Connected to database: " + database);

                this.Execute("CREATE TABLE IF NOT EXISTS `se_indexed_malformed` (`malformed_id` INTEGER PRIMARY KEY AUTOINCREMENT,`malformed_url_id` INTEGER,`malformed_url_content` TEXT);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_indexed_pages` (`page_id` INTEGER PRIMARY KEY AUTOINCREMENT,`page_url_id` INTEGER,`page_html` TEXT);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_indexed_text` (`text_id` INTEGER PRIMARY KEY AUTOINCREMENT,`text_content` TEXT,`text_page_id` TEXT);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_indexed_url` (`url_id` INTEGER PRIMARY KEY AUTOINCREMENT,`url_content` TEXT UNIQUE,`url_crawled` INTEGER DEFAULT 0);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_indexed_words` (`word_id` INTEGER PRIMARY KEY AUTOINCREMENT,`word_content` TEXT NOT NULL,`word_count` INTEGER,`word_page_id` INTEGER);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_exclude_urltype` (`type_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`type_content` TEXT UNIQUE);");
                this.Execute("CREATE TABLE IF NOT EXISTS `se_stopwords` (`stopword_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`stopword_content` TEXT UNIQUE);");

                return false;
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[DatabaseManager][Connect] Unable to connect to database: " + ex.Message);
                return false;
            }
        }

        public void Execute(string sql)
        {
            try
            {
                SQLiteCommand sqliteCommand = this.MasterDatabase.CreateCommand();
                sqliteCommand.CommandType = CommandType.Text;
                sqliteCommand.CommandText = sql;
                SQLiteDataReader sqliteDataReader = sqliteCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[Database.cs][Select] Unable to execute query, will continue anyway.");
            }
        }

        public DataTable Select(string sql, SQLiteParameter[] sqlparams)
        {
            DataTable result;
            try
            {
                SQLiteCommand sqliteCommand = this.MasterDatabase.CreateCommand();
                sqliteCommand.CommandType = CommandType.Text;
                sqliteCommand.CommandText = sql;
                if (sqlparams != null)
                {
                    sqliteCommand.Parameters.AddRange(sqlparams);
                }
                SQLiteDataReader reader = sqliteCommand.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                result = dataTable;
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[Database.cs][Select] Unable to execute query, will continue anyway.");
                result = null;
            }
            return result;
        }

        public void Insert(string sql, SQLiteParameter[] sqlparams)
        {
            try
            {
                SQLiteCommand sqliteCommand = this.MasterDatabase.CreateCommand();
                sqliteCommand.CommandType = CommandType.Text;
                sqliteCommand.CommandText = sql;
                sqliteCommand.Parameters.AddRange(sqlparams);
                SQLiteDataReader sqliteDataReader = sqliteCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[Database.cs][Select] Unable to execute query, will continue anyway.");
            }
        }

        public void Update(string sql, SQLiteParameter[] sqlparams)
        {
            try
            {
                SQLiteCommand sqliteCommand = this.MasterDatabase.CreateCommand();
                sqliteCommand.CommandType = CommandType.Text;
                sqliteCommand.CommandText = sql;
                sqliteCommand.Parameters.AddRange(sqlparams);
                SQLiteDataReader sqliteDataReader = sqliteCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                CrawlerConsole.WriteLineError("[Database.cs][Select] Unable to execute query, will continue anyway.");
            }
        }
    }
}
