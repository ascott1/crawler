﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThunderheadCrawler
{
    static class CrawlerConsole
    {
        public static void WriteLine(string line)
        {
            System.Console.WriteLine(line);
        }

        public static void Write(string line)
        {
            System.Console.Write(line);
        }

        public static void WriteLineInfo(string line)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(line);
            Console.ResetColor();
        }

        public static void WriteLineSuccess(string line)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(line);
            Console.ResetColor();
        }

        public static void WriteLineError(string line)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(line);
            Console.ResetColor();
        }
    }
}
